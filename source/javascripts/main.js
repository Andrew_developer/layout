$(document).ready(function (e) {
    $('.slider').owlCarousel({
        loop:true,
        margin:0,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            1000:{
                items:1,
                nav:true,
                loop:false
            }
        }
    });

    $('.button-menu').click(function (e) {
        $('.mob-menu').slideToggle(500);
    });


    var map = new google.maps.Map(document.getElementById("map"), {center: {lat: -33.863394, lng: 151.209285}, zoom: 16});
    var marker = new google.maps.Marker({
        position: {lat: -33.863394, lng: 151.209285},
        map: map,
        title: '4th Floor, 20 Conduit Street, London, W1S 2XW'
    });
});